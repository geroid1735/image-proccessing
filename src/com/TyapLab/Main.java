package com.TyapLab;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Scanner;
import java.util.concurrent.locks.ReentrantLock;

public class Main {

    public static int nThreads;

    public static void main(String[] args) throws Exception
    {
        BufferedImage image;
        Scanner in = new Scanner(System.in);
        int width;
        int height;
        // обработчик
        int imageStrings = 0;
        System.out.println("Please enter number of threads: ");
        nThreads = in.nextInt();
        Thread[] threads = new Thread[nThreads];
        try {
            File input = new File("image.jpg");
            image = ImageIO.read(input);
            //width= image.getWidth();
            height = image.getHeight();
            imageStrings = height / nThreads;
            ReentrantLock lock = new ReentrantLock();
            try {
                lock.lock();
                StopWatch watch = new StopWatch();
                for (int i = 0; i < nThreads; i++) {
                    threads[i] = new Worker(i * imageStrings, (i + 1) * imageStrings, image, lock);
                    threads[i].start();
                }
                lock.unlock();
                for (int i = 0; i < nThreads; i++) {
                    threads[i].join();
                }
                System.out.println(watch.elapsedTime());
            } catch (Exception ex)
            {
                System.out.println(ex);
            }
            File output = new File("grayscale.jpg");
            ImageIO.write(image, "jpg", output);
        }
        catch (Exception e) {}
        System.out.println("Image is ready! Check it");
    }


}
