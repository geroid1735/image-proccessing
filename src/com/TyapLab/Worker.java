package com.TyapLab;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Worker extends Thread {
    public int start, end;
    public BufferedImage image;
    public ReentrantLock locker;

    //final Condition notFull = locker.newCondition();
    //public Condition notEmpy = locker.newCondition();

    public Worker(int  start, int end, BufferedImage image1, ReentrantLock lock){
        this.start = start;
        this.end = end;
        image = image1;
        locker = lock;

    }

    @Override
    public void run()
    {
        try {
            for (; start < end; start++) {
                for (int i = 0; i < image.getWidth(); i++) {
                    Color c = new Color(image.getRGB(i, start));
                    int grayScale = (int) ((c.getRed() * 0.299) + (c.getGreen() * 0.587) + (c.getBlue() * 0.114));
                    Color newColor = new Color(grayScale, grayScale, grayScale);
                    image.setRGB(i, start, newColor.getRGB());
                }
            }
        } catch(Exception e)
        {
            System.out.println(e);
        }
    }
}
